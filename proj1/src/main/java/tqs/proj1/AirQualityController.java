package tqs.proj1;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;

@RestController
public class AirQualityController {

    private AirQualityService airQualityService;

    public AirQualityController() {
      airQualityService = new AirQualityService();
    }

    @GetMapping("/info")
    public AirQuality city(@RequestParam (required = false) String cityName) throws IOException {
        return airQualityService.getCityName(cityName);
    }

    @GetMapping("/stats")
    public String getStats() {
        return "Requests: " + Cache.getRequests() + "<br>Hits: " + airQualityService.getHit() + "<br>Miss: " + airQualityService.getMiss();
    }

}
