package tqs.proj1;

import java.sql.Timestamp;
import java.util.HashMap;

public class Cache {

    private HashMap<String, AirQuality> airQuality;
    private static int requests = 0;
    private static int hit = 0;
    private static int miss = 0;

    public Cache() {
        this.airQuality = new HashMap<>();
    }

    public AirQuality getInfo(String city) {
        if (hasInfo(city)) {
            return this.airQuality.get(city);
        } else
            return null;
    }

    public boolean hasInfo(String city) {
        boolean tent = false;
        requests++;
        if (this.airQuality.containsKey(city)) {
            hit++;
            tent = true;
        } else
            miss++;
        return tent;
    }

    public boolean timeValid(String city) {
        long curTime = new Timestamp(System.currentTimeMillis()).getTime();
        return (airQuality.containsKey(city) && curTime - airQuality.get(city).getTimestamp() < 250000);
    }

    public void info(String city, AirQuality airQuality) {
        this.airQuality.put(city, airQuality);
    }

    public HashMap<String, AirQuality> getCities() {
        return airQuality;
    }

    public static int getHit() {
        return hit;
    }

    public static int getMiss() {
        return miss;
    }

    public static int getRequests() {
        return requests;
    }

    public static void setHit(int hit) {
        Cache.hit = hit;
    }

    public static void setMiss(int miss) {
        Cache.miss = miss;
    }

    public static void setRequests(int requests) {
        Cache.requests = requests;
    }
}
