package tqs.proj1;

import org.json.simple.JSONObject;

public class ParseJson {

    private ParseJson() {
        throw new IllegalStateException("Utility class");
    }
    public static AirQuality parseJson(JSONObject rawData) {
        String name;
        String aqi;
        String dominentpol;
        String co;
        String h;
        String no2;
        String o3;
        String p;
        String pm10;
        String pm25;
        String so2;
        String t;
        String w;
        AirQuality airQuality;
        String noinformation  = "No Information";

        JSONObject fullInfo = (JSONObject) rawData.get("data");
        JSONObject iaqi = (JSONObject) fullInfo.get("iaqi");
        JSONObject cityInfo = (JSONObject) fullInfo.get("city");

        name = (String) cityInfo.get("name");
        aqi = String.valueOf(fullInfo.get("aqi"));
        dominentpol = (String) fullInfo.get("dominentpol");

        JSONObject infoNO2 = (JSONObject) iaqi.get("no2");
        if(infoNO2 != null)
            no2 = String.valueOf(infoNO2.get("v"));
        else{
            no2 = noinformation;
        }


        JSONObject infoCO = (JSONObject) iaqi.get("co");
        if(infoCO != null)
            co = String.valueOf(infoCO.get("v"));
        else{
            co = noinformation;
        }


        JSONObject infoH = (JSONObject) iaqi.get("h");
        if(infoH != null)
            h = String.valueOf(infoH.get("v"));
        else{
            h = noinformation;
        }


        JSONObject infoO3 = (JSONObject) iaqi.get("o3");
        if(infoO3 != null)
            o3 = String.valueOf(infoO3.get("v"));
        else{
            o3 = noinformation;
        }


        JSONObject infoP = (JSONObject) iaqi.get("p");
        if(infoP != null)
            p = String.valueOf(infoP.get("v"));
        else{
            p = noinformation;
        }


        JSONObject infoPM10 = (JSONObject) iaqi.get("pm10");
        if(infoPM10 != null)
            pm10 = String.valueOf(infoPM10.get("v"));
        else{
            pm10 = noinformation;
        }


        JSONObject infoPM25 = (JSONObject) iaqi.get("pm25");
        if(infoPM25 != null)
            pm25 = String.valueOf(infoPM25.get("v"));
        else{
            pm25 = noinformation;
        }


        JSONObject infoSO2 = (JSONObject) iaqi.get("so2");
        if(infoSO2 != null)
            so2 = String.valueOf(infoSO2.get("v"));
        else{
            so2 = noinformation;
        }


        JSONObject infoT = (JSONObject) iaqi.get("t");
        if(infoT != null)
            t = String.valueOf(infoT.get("v"));
        else{
            t = noinformation;
        }


        JSONObject infoW = (JSONObject) iaqi.get("w");
        if(infoW != null)
            w = String.valueOf(infoW.get("v"));
        else {
            w = noinformation;
        }

        airQuality = new AirQuality(name, aqi, dominentpol, co, h, no2, o3, p, pm10, pm25, so2, t, w);
        return airQuality;
    }
}
