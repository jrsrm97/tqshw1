package tqs.proj1;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Scanner;

public class AirQualityService {
    String apiKey = "6f1b2c08cb1b2613772204a711defb44266c2062";

    private Cache cache;
    private JSONParser jsonParser = new JSONParser();

    public AirQualityService() {
        this.cache = new Cache();
    }

    public AirQuality getCityName(String city) throws IOException {

        if(existsData(city) && cache.timeValid(city)) {
            return cache.getInfo(city);
        }

        if(city == null) {
            return new AirQuality();
        }

        else {
            URL url = new URL("https://api.waqi.info/feed/" + city + "/?token=" + apiKey);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            int status = connection.getResponseCode();
            String apiInfo = "";
            if (status != 200) {
                return new AirQuality();
            } else {
                AirQuality airQuality = new AirQuality();
                try (Scanner sc = new Scanner(url.openStream())) {
                    while (sc.hasNext()) {
                        apiInfo += sc.nextLine();
                    }
                } catch (Exception e) {
                    throw new NullPointerException();
                }

                JSONObject response;
                try {
                    response = (JSONObject) jsonParser.parse(apiInfo);
                    String error = (String) response.get("status");
                    if(error.equals("error")) {
                        return new AirQuality();
                    } else {
                        airQuality = ParseJson.parseJson(response);
                        long currentTime = new Timestamp(System.currentTimeMillis()).getTime();
                        airQuality.setTimestamp(currentTime);
                        saveCity(city, airQuality);
                    }
                } catch (ParseException e) {
                    throw new NullPointerException();
                }
                return airQuality;
            }
        }
    }

    public boolean existsData(String city) {
        return cache.hasInfo(city);
    }

    public void saveCity(String city, AirQuality airQuality) {
        cache.info(city, airQuality);
    }

    public int getHit() {
        return Cache.getHit(); }

    public int getMiss() {
        return Cache.getMiss(); }
}