package tqs.proj1;

public class AirQuality {


    private String name;
    private String aqi;
    private String dominentpol;
    private String co;
    private String h;
    private String no2;
    private String o3;
    private String p;
    private String pm10;
    private String pm25;
    private String so2;
    private String t;
    private String w;
    private long timestamp;

    public AirQuality() {

    }

    public AirQuality(String name, String aqi, String dominentpol, String co, String h, String no2, String o3, String p, String pm10, String pm25, String so2,
                      String t, String w) {
        this.name = name;
        this.aqi = aqi;
        this.dominentpol = dominentpol;
        this.co = co;
        this.h = h;
        this.no2 = no2;
        this.o3 = o3;
        this.p = p;
        this.pm10 = pm10;
        this.pm25 = pm25;
        this.so2 = so2;
        this.t = t;
        this.w = w;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAqi() {
        return aqi;
    }

    public void setAqi(String aqi) {
        this.aqi = aqi;
    }

    public String getDominentpol() {
        return dominentpol;
    }

    public void setDominentpol(String dominentpol) {
        this.dominentpol = dominentpol;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }

    public String getNo2() {
        return no2;
    }

    public void setNo2(String no2) {
        this.no2 = no2;
    }

    public String getO3() {
        return o3;
    }

    public void setO3(String o3) {
        this.o3 = o3;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getPm10() {
        return pm10;
    }

    public void setPm10(String pm10) {
        this.pm10 = pm10;
    }

    public String getPm25() {
        return pm25;
    }

    public void setPm25(String pm25) {
        this.pm25 = pm25;
    }

    public String getSo2() {
        return so2;
    }

    public void setSo2(String so2) {
        this.so2 = so2;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getW() {
        return w;
    }

    public void setW(String w) {
        this.w = w;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
