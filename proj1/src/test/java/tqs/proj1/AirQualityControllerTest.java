package tqs.proj1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.hamcrest.CoreMatchers.is;

@WebMvcTest(AirQualityController.class)
class AirQualityControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AirQualityService airQualityService;

    @Test
    public void ReturnInfoCity_whenGetCity() throws Exception{
        AirQuality airQuality = new AirQuality("Sobreiras-Lordelo do Ouro, Porto, Portugal", "39", "o3", "43", "62,5", "noinfo", "43", "1019", "noinfo", "noinfo", "noinfo", "19", "0.4");
        String city = "porto";

        given(airQualityService.getCityName(city)).willReturn(airQuality);
        mockMvc.perform(get("/info?cityName=" + city))
                .andExpect(jsonPath("name", is(airQuality.getName())))
                .andExpect(jsonPath("dominentpol", is(airQuality.getDominentpol())))
                .andExpect(status().isOk());
    }

    @Test
    public void ReturnNull_whenNotPassAnything() throws Exception {
        AirQuality airQuality = new AirQuality();
        String city = null;
        given(airQualityService.getCityName(city)).willReturn(airQuality);
        mockMvc.perform(get("/info?cityName=" + city))
                .andExpect(jsonPath("$.name", is(airQuality.getName())))
                .andExpect(jsonPath("$.aqi", is(airQuality.getAqi())))
                .andExpect(jsonPath("$.dominentpol", is(airQuality.getDominentpol())))
                .andExpect(status().isOk());
    }

    @Test
    public void ReturnNull_whenCityNotExist() throws Exception {
        AirQuality airQuality = new AirQuality();
        String city = "aveiro";
        given(airQualityService.getCityName(city)).willReturn(airQuality);
        mockMvc.perform(get("/info?cityName=" + city))
                .andExpect(jsonPath("$.name", is(airQuality.getName())))
                .andExpect(jsonPath("$.aqi", is(airQuality.getAqi())))
                .andExpect(jsonPath("$.dominentpol", is(airQuality.getDominentpol())))
                .andExpect(status().isOk());
    }

    @Test
    public void getStats() throws Exception {
        Cache.setHit(1);
        Cache.setMiss(2);
        Cache.setRequests(3);
        String result = mockMvc.perform(get("/stats"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        String[] split = result.split("<br>");
        String requests = split[0];
        String hits = split[1];
        String miss = split[2];

        String[] split2 = requests.split(" ");
        assertThat(Integer.parseInt(split2[1])).isEqualTo(Cache.getRequests());
        String[] split3 = hits.split(" ");
        assertThat(Integer.parseInt(split3[1])).isEqualTo(Cache.getHit());
        String[] split4 = miss.split(" ");
        assertThat(Integer.parseInt(split4[1])).isEqualTo(Cache.getMiss());
    }
}