package tqs.proj1;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class SeleniumUITest {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @AfterEach
    public void pullDown() throws Exception {
        driver.quit();
        String errorString = verificationErrors.toString();
        if (!"".equals(errorString)) {
            Assertions.fail(errorString);
        }
    }
    //Este teste da erro na primeira vez que corre, nao percebi o porque
    @Test
    public void CityIsCorrect() {
        driver.get("http://localhost:8080/");
        driver.findElement(By.id("name")).click();
        driver.findElement(By.id("name")).sendKeys("Porto");
        driver.findElement(By.id("{{ search }}")).click();
        driver.findElement(By.id("aqi"));
        driver.findElement(By.id("dominentpol"));
        assertThat(driver.findElement(By.id("dominentpol")).getText(), is("Dominant Polltuant: o3"));
    }

    @Test
    public void CityIsEmptyString(){
        driver.get("http://localhost:8080/");
        driver.findElement(By.id("name")).click();
        driver.findElement(By.id("name")).sendKeys("");
        driver.findElement(By.id("{{ search }}")).click();
        driver.findElement(By.id("error"));
        assertThat(driver.findElement(By.id("error")).getText(), is(""));
    }

    @Test
    public void CityIsIncorrect(){
        driver.get("http://localhost:8080/");
        driver.findElement(By.id("name")).click();
        driver.findElement(By.id("name")).sendKeys("Aveiro");
        driver.findElement(By.id("{{ search }}")).click();
        driver.findElement(By.id("error"));
        assertThat(driver.findElement(By.id("error")).getText(), is(""));
    }

}
