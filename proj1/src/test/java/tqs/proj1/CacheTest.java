package tqs.proj1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CacheTest {
    private Cache cache;

    private AirQuality airQuality;
    @BeforeEach
    void setUp() {
        cache = new Cache();
        Cache.setHit(0);
        Cache.setMiss(0);
        Cache.setRequests(0);
        airQuality = new AirQuality("porto", "42", "O3", "43", "62,5", "noinfo", "43", "1019", "noinfo", "noinfo", "noinfo", "19", "0.4");

    }

    @Test
    void getInfoWhenCityIsInCache() {
        cache.info("porto", airQuality);
        AirQuality city = cache.getInfo("porto");
        assertThat(airQuality.getName()).isEqualTo(city.getName());
    }

    @Test
    void getInfoWhenCityIsNotInCache() {
        AirQuality city = cache.getInfo("porto");
        assertNull(city);
    }

    @Test
    void hasInfo() {
        cache.info("porto", airQuality);
        assertThat(true).isEqualTo(cache.getCities().containsKey("porto"));
    }

    @Test
    public void timeValid() {
        long currentTime = new Timestamp(System.currentTimeMillis()).getTime();
        airQuality.setTimestamp(currentTime);
        cache.info("porto", airQuality);
        boolean result = cache.timeValid("porto");
        assertThat(true).isEqualTo(result);
    }

    @Test
    public void timeIsNotValid() {
        long currentTime = new Timestamp(System.currentTimeMillis()).getTime() - 350000;
        airQuality.setTimestamp(currentTime);
        cache.info("porto", airQuality);
        boolean result = cache.timeValid("porto");
        assertThat(false).isEqualTo(result);
    }


    @Test
    void info() {
        int size = 1;
        cache.info("porto", airQuality);
        assertThat(1).isEqualTo(cache.getCities().size());
    }

    @Test
    void IncMisswhenInfoIsNotInCache() {
        cache.getInfo("porto");
        cache.getInfo("lisboa");
        cache.getInfo("madrid");
        assertThat(3).isEqualTo(Cache.getMiss());
    }

    @Test
    void IncHitwhenInfoIsNotInCache() {
        cache.info("porto", airQuality);
        cache.getInfo("porto");
        assertThat(1).isEqualTo(Cache.getHit());
    }
    @Test
    void IncRequestwhenInfoIsNotInCache() {
        cache.info("porto", airQuality);
        cache.getInfo("porto");
        cache.getInfo("lisboa");
        assertThat(2).isEqualTo(Cache.getRequests());
    }

}