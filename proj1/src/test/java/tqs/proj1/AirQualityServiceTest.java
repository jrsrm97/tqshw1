package tqs.proj1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
class AirQualityServiceTest {
    @Mock(lenient = true)
    private Cache cache;

    @InjectMocks
    private AirQualityService airQualityService;

    private AirQuality airQuality;
    private AirQuality airQuality1;
    int miss;
    int hits;
    String city;

    @BeforeEach
    void setUp() {
        city = "Sobreiras-Lordelo do Ouro, Porto, Portugal";
        cache = new Cache();
        airQuality = new AirQuality("Sobreiras-Lordelo do Ouro, Porto, Portugal", "40", "o3", "43", "62,5", "noinfo", "43", "1019", "noinfo", "noinfo", "noinfo", "19", "0.4");
        airQuality1 = new AirQuality();
        hits =3;
        miss =4;
    }


    @Test
    void VerifyIfCityIsInCache() throws Exception {
        String city = "porto";
        cache.info("Sobreiras-Lordelo do Ouro, Porto, Portugal", airQuality);


        cache.info("Sobreiras-Lordelo do Ouro, Porto, Portugal", airQuality);
        AirQuality airQuality1 = cache.getInfo("Sobreiras-Lordelo do Ouro, Porto, Portugal");
        assertThat(true).isEqualTo(cache.hasInfo("Sobreiras-Lordelo do Ouro, Porto, Portugal"));
        assertThat(airQuality.getName()).isEqualTo(airQuality1.getName());
    }

    @Test
    public void cityIsNull() throws Exception {
        AirQuality airQuality2 = airQualityService.getCityName(null);
        assertThat(airQuality1.getName()).isEqualTo(airQuality2.getName());
        assertThat(airQuality1.getAqi()).isEqualTo(airQuality2.getAqi());
        assertThat(airQuality1.getDominentpol()).isEqualTo(airQuality2.getDominentpol());
        assertThat(airQuality1.getCo()).isEqualTo(airQuality2.getCo());
        assertThat(airQuality1.getH()).isEqualTo(airQuality2.getH());
        assertThat(airQuality1.getNo2()).isEqualTo(airQuality2.getNo2());
        assertThat(airQuality1.getO3()).isEqualTo(airQuality2.getO3());
        assertThat(airQuality1.getP()).isEqualTo(airQuality2.getP());
        assertThat(airQuality1.getPm10()).isEqualTo(airQuality2.getPm10());
        assertThat(airQuality1.getPm25()).isEqualTo(airQuality2.getPm25());
        assertThat(airQuality1.getSo2()).isEqualTo(airQuality2.getSo2());
        assertThat(airQuality1.getT()).isEqualTo(airQuality2.getT());
        assertThat(airQuality1.getW()).isEqualTo(airQuality2.getW());
    }

    @Test
    public void dontHaveInformationForSomeParameters() throws Exception {
        String city = "porto";
        AirQuality airQuality3 = airQualityService.getCityName(city);
        assertThat(airQuality3.getName()).isEqualTo(airQuality.getName());
        assertThat(airQuality3.getDominentpol()).isEqualTo(airQuality.getDominentpol());
    }

    @Test
    public void existsData() throws Exception {
        cache.info(city, airQuality);
        cache.getInfo(city);
        assertThat(true).isEqualTo(cache.hasInfo(city));
    }

    @Test
    public void notExistsData() throws Exception {
        String city1 = "blabla";
        cache.info(city, airQuality);
        cache.getInfo(city);
        assertThat(false).isEqualTo(cache.hasInfo(city1));
    }


    @Test
    public void getHit(){
        cache.setHit(3);
        assertThat(cache.getHit()).isEqualTo(hits);
    }

    @Test
    public void getMiss(){
        cache.setMiss(4);
        assertThat(cache.getMiss()).isEqualTo(miss);
    }

}